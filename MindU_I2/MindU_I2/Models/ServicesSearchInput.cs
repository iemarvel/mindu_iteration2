﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MindU_I2.Models
{
    public class ServicesSearchInput
    {
        // This property will hold a category, selected by user
        public string Category { get; set; }
        public string Postcode { get; set; }
        // This property will hold all available categories for selection
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}