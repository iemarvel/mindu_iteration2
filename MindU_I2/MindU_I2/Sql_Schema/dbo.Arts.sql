﻿CREATE TABLE [dbo].[Arts] (
    [aid]      INT           NOT NULL,
    [name]     VARCHAR (50)  NOT NULL,
    [address]  VARCHAR (100) NOT NULL,
    [suburb]   VARCHAR (50)  NOT NULL,
    [postcode] VARCHAR (50)  NOT NULL,
    [state]    VARCHAR (50)  NOT NULL,
    [category] VARCHAR (50)  NOT NULL,
    [website]  VARCHAR (50)  NOT NULL,
    PRIMARY KEY CLUSTERED ([aid] ASC)
);

