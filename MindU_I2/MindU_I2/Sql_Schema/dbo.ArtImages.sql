﻿CREATE TABLE [dbo].[ArtImages] (
    [imgId]   INT            NOT NULL,
    [imgPath] NVARCHAR (MAX) NOT NULL,
    [aid]     INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([imgId] ASC),
    CONSTRAINT [FK_ArtImages_ToArtSpaces] FOREIGN KEY ([aid]) REFERENCES [dbo].[Arts] ([aid])
);

