﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MindU_I2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace MindU_I2.Controllers
{
    public class ServicesController : Controller
    {

        // GET: Search
        // 1. Action method for displaying 'Search' page
        public ActionResult SearchTest1()
        {
            // Let's get all categories that we need for a DropDownList
            var categories = GetAllCatogories();
            var model = new ServicesSearchInput();
            // Create a list of SelectListItems so these can be rendered on the page
            model.Categories= GetSelectListItems(categories);
            return View(model);
        }

        //Post: Search
        // 2. Action method for handling user-entered data when 'Search' button is pressed.
        public ActionResult SearchResult(ServicesSearchInput model)
        {
            // Get all categories again
            var categories = GetAllCatogories();
            // Set these categories on the model. We need to do this because
            // only the selected value from the DropDownList is posted back, not the whole list of categories
            model.Categories = GetSelectListItems(categories);
            // In case everything is fine - i.e. "category" entered/selected,
            // redirect user to the "Search Result" page, and pass the user object along via Session
            if (ModelState.IsValid)
            {
                //do what SearchResult does
                Session["ServicesSearchInput"] = model;
                // Display SearchResult.html page that shows results associated with selected category.
                string apiKey = "AIzaSyC-XG2JrTbessPRoSMRbasmtCwBY8oR2iI";
                string searchCategory = "Psychologist";
                if (model.Category != null) {
                    searchCategory = model.Category;
                    if (searchCategory.Contains(" "))
                    {
                        searchCategory.Replace(" ", "+");
                    }
                }              
                var request = WebRequest.Create("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + searchCategory + "+in+Vic" + "&key=" + apiKey);
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    string responseString = reader.ReadToEnd();
                    dynamic jsonData = JsonConvert.DeserializeObject(responseString);
                    var venues = new List<Venue>();
                    foreach (var item in jsonData.results)
                    {
                        string ratingx = item.rating;
                        if (ratingx.Contains("5"))
                        {
                            venues.Add(new Venue
                            {
                                name = item.name,
                                formatted_address = item.formatted_address,
                                rating = item.rating,
                                place_id = item.place_id,
                                lat = item.geometry.location.lat,
                                lng = item.geometry.location.lng
                            });
                        }
                    }
                    foreach (var venue in venues)
                    {
                        request = WebRequest.Create("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + venue.place_id + "&fields=formatted_phone_number,website&key=" + apiKey);
                        response = (HttpWebResponse)request.GetResponse();
                        dataStream = response.GetResponseStream();
                        reader = new StreamReader(dataStream);
                        responseString = reader.ReadToEnd();
                        jsonData = JsonConvert.DeserializeObject(responseString);
                        venue.formatted_phone_number = jsonData["result"]["formatted_phone_number"];
                        venue.website = jsonData["result"]["website"];

                    }
                    return View(venues);
                }
                catch (Exception e){
                    return View("Search", model);
                }
                //return RedirectToAction("SearchResult");
            }
            // Something is not right - so render the search page again,
            // keeping the data user has entered by supplying the model.
            return View("Search", model);
        }
     
        // Just return a list of categories - in a real-world application this would call
        // into data access layer to retrieve states from a database.
        private IEnumerable<string> GetAllCatogories()
        {
            return new List<string>
            {
                "Psychologist",
                "Psychological Consultant",
                "Psychological Clinic",
                "Community Mental Health Service",
                "Mental Health Service 24 Hours help line"
            };
        }

        // This function takes a list of strings and returns a list of SelectListItem objects.
        // These objects are going to be used later in the SignUp.html template to render the
        // DropDownList.
        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            // Create an empty list to hold result of the operation
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="State Name">State Name</option>
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
        public ActionResult ConditionsInfo()
        {
            return View();
        }

        public ActionResult MentalHealthContext()
        {
            return View();
        }
        public ActionResult Tips()
        {
            return View();
        }
        public ActionResult Search(string category)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Please choose category", Value = "Please choose category" });
            items.Add(new SelectListItem { Text = "Psychological Consultant", Value = "Psychological Consultant" });
            items.Add(new SelectListItem { Text = "Psychological Clinic", Value = "Psychological Clinic" });
            items.Add(new SelectListItem { Text = "Psychologist", Value = "Psychologist" });
            ViewBag.defaultSelect = "Please choose category";
            ViewBag.selectList = items;
            // Display SearchResult.html page that shows results associated with selected category.
            string apiKey = "AIzaSyC-XG2JrTbessPRoSMRbasmtCwBY8oR2iI";
            var venues = new List<Venue>();
            if (category == null) {
                ViewBag.result = 0;
                return View(venues);
                
            }
            if (category.Contains(" "))
            {
                category.Replace(" ", "+");
            }
            var request = WebRequest.Create("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + category + "+in+Vic" + "&key=" + apiKey);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseString = reader.ReadToEnd();
            dynamic jsonData = JsonConvert.DeserializeObject(responseString);
            //var venues = new List<Venue>();
            if (jsonData.results == null)
            {
                ViewBag.result = 1;
                return View(venues);
            }
            foreach (var item in jsonData.results)
            {
                string ratingx = item.rating;
                if (ratingx.Contains("5"))
                {
                    venues.Add(new Venue
                    {
                        name = item.name,
                        formatted_address = item.formatted_address,
                        rating = item.rating,
                        place_id = item.place_id,
                        lat = item.geometry.location.lat,
                        lng = item.geometry.location.lng
                    });
                }
            }

            foreach (var venue in venues)
            {
                request = WebRequest.Create("https://maps.googleapis.com/maps/api/place/details/json?placeid=" + venue.place_id + "&fields=formatted_phone_number,website&key=" + apiKey);
                response = (HttpWebResponse)request.GetResponse();
                dataStream = response.GetResponseStream();
                reader = new StreamReader(dataStream);
                responseString = reader.ReadToEnd();
                jsonData = JsonConvert.DeserializeObject(responseString);
                venue.formatted_phone_number = jsonData["result"]["formatted_phone_number"];
                venue.website = jsonData["result"]["website"];

            }
            return View(venues);
        }


        }
    }
