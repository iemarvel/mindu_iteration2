﻿// Bar chart
new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
        labels: ["Stressed", "Lack of energy", "Anxious", "Low mood", "Feelings of Hopelessness", "Trouble sleeping", "Panic", "Thoughts of self-harm or suicide"],
        datasets: [
            {
                label: "Percentage",
                backgroundColor: "#3e95cd", 
                data: [83.2, 82.1, 79.0, 75.8, 59.2, 55.6, 52.7, 35.5]
            }
        ]
    },
    options: {
        legend:
        {
            display: true,
            position: 'top'
        },
        title: {
            display: true,
            text: 'The Mental Health Conditions of Teritiary Students Aged 17 to 25 in Australia'
        },
        scales: {
            xAxes: [{
                ticks: {
                    autoSkip: false
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});