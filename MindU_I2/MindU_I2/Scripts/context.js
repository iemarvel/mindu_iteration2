﻿// Bar chart
new Chart(document.getElementById("bar-chart1"), {
    type: 'bar',
    data: {
        labels: ["Behavioural disorders", "Depression", "Affective disorders", "Anxiety disorders", "Adjustment disorders", "Eating disorders", "Personality disorders"],
        datasets: [
            {
                label: "No. of Contacts in VIC",
                backgroundColor: "#3e95cd", 
                data: [64764, 139096, 7937, 63177, 74979, 33905, 107128, 1330267]
            },
            {
                label: "No. of Contacts in Australia",
                backgroundColor: "#f4b642",
                data: [320918, 800674, 38879, 306747, 334373, 104055, 326906, 7137807]
            }
        ]
    },
    options: {
        legend:
        {
            display: true,
            position: 'top'
        },
        title: {
            display: true,
            text: 'Community Mental Health Care Service Contacts by Principal Diagnosis'
        },
        scales: {
            xAxes: [{
                ticks: {
                    autoSkip: false
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: 900000,

                }
            }]
        }
    }
});

//Pie Chart
new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
        labels: ["Behavioural disorders", "Depression", "Affective disorders", "Anxiety disorders", "Adjustment disorders", "Eatring disorders", "Personality disorder", "Other"],
        datasets: [{
            label: "Percentage",
            backgroundColor: ["#3e95cd", "#b75858", "#3cba9f", "#45b0d1", "#8d8eb7", "#b889c4", "#f9e39f", "#ff6b6b"], 
            data: [3.56, 7.64, 0.44, 3.47, 4.12, 1.86, 5.88, 73.04]
        }]
    },
    options: {
        legend:
        {
            display: true,
            position: 'bottom'
        },
        title: {
            display: true,
            text: 'The Percentage of People Who Have Common Disorders in VIC'
        }
    }
});